using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace SportsTranslator
{
    /// <summary>
    /// This class shall check and create folders that does not exists
    /// TODO: See if we can use this to also check in which folders we are getting the data from
    /// 
    /// </summary>
    class folders
    {
        public string InputDirectory = "";
		public string OutputDirectory = "";
		public string DoneDirectory = "";
		public string ErrorDirectory = "";
        public string LogDirectory = "";
		public string Dictionary = "";
        public string DicDirectory = "";
        public string Name = "";

        public folders()
        {
            // 
        }

        public void setFolders(string name)
		{
			this.Name = name;
			SetPaths();
		}

		private void SetPaths()
		{
            this.InputDirectory = CompletePath(Properties.Settings.Default.InputPath, false);
            this.OutputDirectory = CompletePath(Properties.Settings.Default.OutputPath, false);
            this.ErrorDirectory = CompletePath(Properties.Settings.Default.ErrorPath, false);
            this.DoneDirectory = CompletePath(Properties.Settings.Default.DonePath, false);
            this.LogDirectory = CompletePath(Properties.Settings.Default.LogPath, false);
            this.DicDirectory = CompletePath(Properties.Settings.Default.DictionaryPath, false);
		}

		private string CompletePath( string parentDir, bool useSubirectories )
		{
			string tmp = parentDir;

			if( useSubirectories )
			{
				tmp += @"\" + this.Name;
			}
			
			return tmp;
		}

        private Array LoadFolders () {

            //string folder= "";
            //string type = "";
            //string sub = "";

            //string FolderFile = Properties.Settings.Default.FolderFile;

            //XmlDocument xml = new XmlDocument();

            //try
            //{
            //    // Loading the Folder file that contains a lot of information
            //    xml.Load(FolderFile);

            //    XmlNodeList nodes = null;
            //    //  XmlNode node = null;

            //    nodes = xml.SelectNodes(@"/folderJobs/job");
            //    string tet = "";

                
            //    foreach (XmlNode node in nodes) {
            //    job_map (node.Attributes("code").Value) = node.Attributes("name").Value;
            //    }

                
            //    //foreach (string non) {
            //    //    folder_map (node.Attributes("type").value) = node.Attributes("folder").value;
            //    //}
            //}
            //catch
            //{

            //}

            /*
             * 
            For Each node In nodes
                
            Next

            WriteLog(logFolder, "Nation map loaded: " & nation_map.Count & " nations.")
        Catch ex As Exception
            'Error loading nation map
            WriteErr(logFolder, "Error nation map (" & nationmapfile & ") file.", ex)
        End Try
             */
            return null;
        }
    }
}