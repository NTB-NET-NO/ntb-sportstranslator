using System;
using System.Collections.Generic;
using System.Text;

namespace SportsTranslator
{
    class Convert
    {
        public string ConvertString(string data, string EncoderType)
        {
            // Create two different encodings.
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;
            Encoding utf8 = Encoding.UTF8;
            Encoding iso = Encoding.GetEncoding("ISO-8859-1");

            // Convert the string into a byte[].
            byte[] unicodeBytes = unicode.GetBytes(data);
            byte[] isoBytes = iso.GetBytes(data);
            byte[] utf8Bytes = utf8.GetBytes(data);
            byte[] asciiBytes = ascii.GetBytes(data);

            // Perform the conversion from one encoding to the other.
            if (EncoderType == "iso")
            {
                
            }

            byte[] ConvertBytes = Encoding.Convert(unicode, iso, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            // This is a slightly different approach to converting to illustrate
            // the use of GetCharCount/GetChars.
            char[] isoChars = new char[iso.GetCharCount(isoBytes, 0, isoBytes.Length)];
            iso.GetChars(isoBytes, 0, isoBytes.Length, isoChars, 0);
            string isoString = new string(isoChars);

            // Display the strings created before and after the conversion.

            return isoString;
        }
    }
}
