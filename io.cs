#define DEBUG
using System;
using System.IO;
using System.Collections;
using System.Management;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using ntb_FuncLib;



namespace SportsTranslator
{
	/// <summary>
	/// This class deals with everything that has to do with IO.
	/// For instance creating directories, loading files, saving files and so on
	/// </summary>
    /// 
	public class io
	{
        private static string dirInput = Properties.Settings.Default.InputPath;
        private static string dirOutput = Properties.Settings.Default.OutputPath;
        private static string dirDictionary = Properties.Settings.Default.DictionaryPath;
        private static string dirLog = Properties.Settings.Default.LogPath;
        private static string dirError = Properties.Settings.Default.ErrorPath;
        private static string dirDone = Properties.Settings.Default.DonePath;

        private string message = "";



		public io()
		{
			/*
             * TODO: Add constructor logic here
             * TODO: Create code so that we can have inputfolders and output-folders (V2.0)
             */


		}

		/// <summary>
		/// This method reads the content of the file
		/// </summary>
		/// <param name="Filename">String containing the name of the file to be read</param>
		/// <returns>Content of the file</returns>
		public string readFile (string Filename) 
		{
			if (!File.Exists(Filename) )
			{
				throw (new FileNotFoundException(
					Filename + " cannot be read since it does not exist.", Filename));
			}

			string contents = "";


			using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
					   FileAccess.Read, FileShare.None)) 
			{
				using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.GetEncoding("ISO-8859-1")))
				{
					contents = streamReader.ReadToEnd();
				}
			}

			return contents;


		}

        

		/// <summary>
		/// This method writes the file to the directory
		/// </summary>
		/// <param name="Filename">String containing the filename</param>
		/// <param name="data">String containing the data that shall be written to the file</param>
		public void writeFile (string Filename, string data) 
		{
            try
            {
                Filename = Filename.Replace("�", "ae");
                Filename = Filename.Replace("�", "oe");
                Filename = Filename.Replace("�", "aa");
                Filename = Filename.Replace("�", "Ae");
                Filename = Filename.Replace("�", "Oe");
                Filename = Filename.Replace("�", "Aa");
                Filename = Filename.Replace("�", "aaa");
                Filename = Filename.Replace("�", "ooo");

                FileInfo newFile = new FileInfo(Filename);
                if (newFile.Exists == true)
                {
                    newFile.Delete();
                }


                
                using (FileStream fileStream = new FileStream(Filename, FileMode.CreateNew,
                           FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter streamWriter = new StreamWriter(fileStream,  Encoding.UTF8)) 
                    {
                        
                        streamWriter.WriteLine(data);
                    }
                }
            }
            catch (IOException iox)
            {
                message = "An error has occured!";
                Exception ie = iox;
                LogFile.WriteErr(ref dirLog, ref message, ref ie);

                if (iox.InnerException.ToString() != "")
                {
                    message = "An inner exceptoion has occured!";
                    Exception e = iox.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref e);
                }

            }
            catch (Exception e)
            {
                message = "An error has occured!";
                LogFile.WriteErr(ref dirLog, ref message, ref e);

                if (e.InnerException.ToString() != "")
                {
                    message = "An inner exception has occured!";
                        Exception ie = e.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref ie);
                }

            }
		}

		/// <summary>
		/// This method checks if the file exists or not
		/// </summary>
		/// <param name="Filename">Filename we are to check existance of</param>
		/// <returns>Returns false if the file exists, otherwize true</returns>
		public bool FileExists (string Filename) 
		{
			if (File.Exists(Filename)) 
			{
				return true;
			} 
			return false;
		}

        public ArrayList ReadIntoArray(string Filename)
        {
            // Creating a string
            string[] strWords = null;


            // Creating the dictionary hashtable (array)
            ArrayList arrContent = new ArrayList();

            try
            {

                if (!File.Exists(Filename))
                {
                    throw (new FileNotFoundException(
                        Filename + " cannot be read since it does not exist.", Filename));

                }


                using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
                           FileAccess.Read, FileShare.None))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.GetEncoding("iso-8859-1")))
                    {
                        while (streamReader.Peek() != -1)
                        {
                            /*
                             * We know that the format of the line is like this:
                             * Word1:ReplaceWord1
                             */
                            arrContent.Add(streamReader.ReadLine());
                            // strContents = ConvertString(strContents);

                        }

                    }
                }
                return arrContent;
            }



            catch (IndexOutOfRangeException iore)
            {
                // Written errors to errorlog
                string message;
                message = "We most likely end up here because languages isn't separated with colon.";
                message += "String words 0 contained: " + strWords[0];

                // string error = iore.ToString();
                Exception error = iore;
                LogFile.WriteErr(ref dirLog, ref message, ref error);

                return null;

            }
            catch (FileNotFoundException ffe)
            {
                string message = "The file was not found!";
                Exception fe = ffe;
                LogFile.WriteErr(ref dirLog, ref message, ref fe);

                return null;
            }
            catch (IOException ioe)
            {
                string message = "An IO exception has occured";

                Exception error = ioe;
                LogFile.WriteErr(ref dirLog, ref message, ref error);

                if (ioe.InnerException.ToString() != "")
                {
                    message = "The inner Exception is";
                    Exception InnerException = ioe.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return null;
            }
            catch (Exception e)
            {
                string message = "An error has occured!";
                LogFile.WriteErr(ref dirLog, ref message, ref e);

                if (e.InnerException.ToString() != "")
                {
                    message = "An Inner Exception has occured";

                    Exception InnerException = e.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return null;
            }
        }


        public string DirectTranslate(string Filename, string strContent)
        {
            // Creating a string
            string strContents = "";
            string[] strWords = null;

            
            try
            {
                // just returning here to debug
                int counter = 0;
                if (!File.Exists(Filename))
                {
                    message = "File " + Filename + " does not exists!";
                    LogFile.WriteLog(ref dirLog, ref message);

                    throw (new FileNotFoundException(
                        Filename + " cannot be read since it does not exist.", Filename));

                }


                using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
                           FileAccess.Read, FileShare.None))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.GetEncoding("iso-8859-1")))
                    {
                        while (streamReader.Peek() != -1)
                        {
                            /*
                             * We know that the format of the line is like this:
                             * Word1:ReplaceWord1
                             */
                            strContents = streamReader.ReadLine();


                            // I am separating different parts in the file with ---Text---
                            // We are also checking for empty lines
                            if ((strContents.Substring(0, 3) != "---") &&
                                (strContents.Length >= 0))
                            {
                                counter++;
                                strWords = strContents.Split(':');

                                string strTemp = strWords[0];
                                string temp2 = strTemp.Replace("(", @"\(");
                                string temp3 = temp2.Replace(")", @"\)");

                                strTemp = temp3;

                                // Doing the replace
                                string regexpattern = "";

                                if (strWords[0].Contains(@"(f)")
                                    ||
                                    (strWords[0].Contains(@"(k)"))
                                    ||
                                    (strWords[0].Contains(@"(k/f)")))
                                {

                                    regexpattern = @"\b" + strTemp;
                                }
                                else
                                {
                                    regexpattern = @"\b" + strTemp + @"\b";
                                }


                                
                                LogFile.WriteLog(ref dirLog, ref regexpattern);
                                strTemp = "";

                                message = counter.ToString() + ": regexp - >" + regexpattern.ToString();
                                LogFile.WriteLog(ref dirLog, ref message);

                                //strContent = 
                                //    ReplaceEx(strContent, regexpattern, strContent);


                                if (Regex.IsMatch(strContent, regexpattern, 
                                    RegexOptions.ExplicitCapture |
                                    RegexOptions.IgnoreCase))
                                {
                                    strContent = Regex.Replace(strContent, regexpattern, strWords[1].ToString(), RegexOptions.IgnoreCase);
                                }

                                regexpattern = @"([0-9]{1}) ([0-9]{3})";
                                if (Regex.IsMatch(strContent, regexpattern, RegexOptions.IgnoreCase))
                                {
                                    strContent = Regex.Replace(strContent, regexpattern, "$1$2", RegexOptions.IgnoreCase);
                                }
                                
                                
                                    // above 10000 = 10.000
                                    regexpattern = @"([0-9]{2}) ([0-9]{3})";
                                if (Regex.IsMatch(strContent, regexpattern))
                                {
                                    strContent = Regex.Replace(strContent, regexpattern, "$1.$2", RegexOptions.IgnoreCase);
                                }

                                // This didn't work, well it did, but the result was catastrofical

                                //if (strContent.Contains(strWords[0].ToString()))
                                //{
                                //    message = "Found " + strWords[0].ToString() + " in string!";
                                //    LogFile.WriteLog(ref dirLog, ref message);
                                //    strContent = strContent.Replace(strWords[0], strWords[1]);
                                //}

                            }

                        }
                    }
                }
                return strContent;
            }
            catch (IndexOutOfRangeException iore)
            {
                // Written errors to errorlog
                string message;
                message = "We most likely end up here because languages isn't separated with colon.";
                message += "String words 0 contained: " + strWords[0];

                // string error = iore.ToString();
                Exception error = iore;
                LogFile.WriteErr(ref dirLog, ref message, ref error);

                return strContent;

            }
            catch (IOException ioe)
            {
                string message = "An IO exception has occured";

                Exception error = ioe;
                LogFile.WriteErr(ref dirLog, ref message, ref error);

                if (ioe.InnerException.ToString() != "")
                {
                    message = "The inner Exception is";
                    Exception InnerException = ioe.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return strContent;
            }
            catch (Exception e)
            {
                string message = "An error has occured!";
                LogFile.WriteErr(ref dirLog, ref message, ref e);

                if (e.InnerException.ToString() != "")
                {
                    message = "An Inner Exception has occured";

                    Exception InnerException = e.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return strContent;
            }
        }

        private static string ReplaceEx(string original,
                    string pattern, string replacement)
        {
            int count, position0, position1;
            count = position0 = position1 = 0;
            string upperString = original.ToUpper();
            string upperPattern = pattern.ToUpper();
            int inc = (original.Length / pattern.Length) *
                      (replacement.Length - pattern.Length);
            char[] chars = new char[original.Length + Math.Max(0, inc)];
            while ((position1 = upperString.IndexOf(upperPattern,
                                              position0)) != -1)
            {
                for (int i = position0; i < position1; ++i)
                    chars[count++] = original[i];
                for (int i = 0; i < replacement.Length; ++i)
                    chars[count++] = replacement[i];
                position0 = position1 + pattern.Length;
            }
            if (position0 == 0) return original;
            for (int i = position0; i < original.Length; ++i)
                chars[count++] = original[i];
            return new string(chars, 0, count);
        }


		/// <summary>
		/// This method reads the content of a file into an array/collection
		/// </summary>
		/// <param name="Filename">name of the file we are to load</param>
		/// <returns>Returns the content of the file in an array</returns>
        /// 
		public Hashtable ReadFileToArray (string Filename) 
		{
            // Creating a string
            string strContents = "";
            string[] strWords = null;


            // Creating the dictionary hashtable (array)
            Hashtable dictionary = new Hashtable();


            try
            {
                // just returning here to debug

                if (!File.Exists(Filename))
                {
                    message = "File " + Filename + " does not exists!";
                    LogFile.WriteLog(ref dirLog, ref message);

                    throw (new FileNotFoundException(
                        Filename + " cannot be read since it does not exist.", Filename));

                }


                using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
                           FileAccess.Read, FileShare.None))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.GetEncoding("iso-8859-1")))
                    {
                        while (streamReader.Peek() != -1)
                        {
                            /*
                             * We know that the format of the line is like this:
                             * Word1:ReplaceWord1
                             */
                            strContents = streamReader.ReadLine();
                            // strContents = ConvertString(strContents);

                            // I am separating different parts in the file with ---Text---
                            // We are also checking for empty lines
                            if ((strContents.Substring(0, 3) != "---") &&
                                (strContents.Length >= 0))
                            {
                                strWords = strContents.Split(':');

                                int Count = strWords.Length;



                                if (!dictionary.Contains(strWords[0]))
                                {
                                    if (Count == 1)
                                    {
                                        strWords[1] = "";
                                    }

                                    dictionary.Add(strWords[0], strWords[1]);
                                }

                            }

                        }
                    }
                }
                return dictionary;
            }
            catch (IndexOutOfRangeException iore)
            {
                // Written errors to errorlog
                string message;
                message = "We most likely end up here because languages isn't separated with colon.";
                message += "String words 0 contained: " + strWords[0];

                // string error = iore.ToString();
                Exception error = iore;
                LogFile.WriteErr(ref dirLog, ref message, ref error);
                    
                return dictionary;

            }
            catch (IOException ioe)
            {
                string message = "An IO exception has occured";

                Exception error = ioe;
                LogFile.WriteErr(ref dirLog, ref message,ref error);

                if (ioe.InnerException.ToString() != "")
                {
                    message = "The inner Exception is";
                    Exception InnerException = ioe.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return dictionary;
            }
            catch (Exception e)
            {
                string message = "An error has occured!";
                LogFile.WriteErr (ref dirLog, ref message, ref e);

                if (e.InnerException.ToString() != "")
                {
                    message = "An Inner Exception has occured";

                    Exception InnerException = e.InnerException;
                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }

                return dictionary;
            }
            

		}

		/// <summary>
		/// This method creates a directory if it does not exists
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void createDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;
			if (!Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);
				dirInfo.Create();
			}
		}

		/// <summary>
		/// This method checks if the directory exists or not
		/// </summary>
		/// <param name="DirectoryName">Name of directory to check - complete path</param>
		/// <returns>Returns false if the directory does not exists, otherwize true</returns>
		public bool getDirectory (string DirectoryName) 
		{
			if (!Directory.Exists(@DirectoryName)) 
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// This method deletes the directory and subdirectories within
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void deleteDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;

			if (Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);

				dirInfo.Delete(true);
			}
		}

        /// <summary>
        /// This method shall move a file from one directory to another
        /// </summary>
        /// <param name="FromDirectory">This string holds the path for the from directory</param>
        /// <param name="ToDirectory">This string holds the path for the to directory</param>
        public void MoveFile(string FromDirectory, string ToDirectory, string fileName)
        {
            FileInfo fileInfo = null;

            if (Directory.Exists(FromDirectory))
            {
                if (Directory.Exists(ToDirectory))
                {
                    fileInfo.MoveTo(ToDirectory + @"\" + fileName);
                }
            }
        }

        /// <summary>
        /// This method is a overload of the prior method and moves the file
        /// </summary>
        /// <param name="ToDirectory">This string holds the name of the directory to move the file</param>
        /// <param name="fileName">This string holds the name of the file we are to move</param>
        public void MoveFile(string OldFilename, string NewFilename)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(OldFilename);
                FileInfo newFile = new FileInfo(NewFilename);

                if (newFile.Exists == true)
                {
                    newFile.Delete();
                }
                fileInfo.MoveTo(NewFilename);
            }
            catch (IOException ioex)
            {
                string message = " An IO Exception has occured!";
                Exception e = ioex;
                LogFile.WriteErr(ref dirLog, ref message, ref e);
            }
            catch (Exception ex)
            {
                string message = "An error has occured!";
                LogFile.WriteErr(ref dirLog, ref message, ref ex);
            }
        }

        public void getDirectoryNames()
        {
            dirInput = Properties.Settings.Default.InputPath;
            dirOutput = Properties.Settings.Default.OutputPath;
            dirDictionary = Properties.Settings.Default.DictionaryPath;
            dirLog = Properties.Settings.Default.LogPath;
            dirError = Properties.Settings.Default.ErrorPath;
            dirDone = Properties.Settings.Default.DonePath;
        
        }

        public static Encoding GetFileEncoding(String FileName)
        // Return the Encoding of a text file.  Return Encoding.Default if no Unicode
        // BOM (byte order mark) is found.
        {
            Encoding Result = null;

            FileInfo FI = new FileInfo(FileName);

            FileStream FS = null;

            try
            {
                FS = FI.OpenRead();

                Encoding[] UnicodeEncodings = { Encoding.BigEndianUnicode, Encoding.Unicode, Encoding.UTF8 };

                for (int i = 0; Result == null && i < UnicodeEncodings.Length; i++)
                {
                    FS.Position = 0;

                    byte[] Preamble = UnicodeEncodings[i].GetPreamble();

                    bool PreamblesAreEqual = true;

                    for (int j = 0; PreamblesAreEqual && j < Preamble.Length; j++)
                    {
                        PreamblesAreEqual = Preamble[j] == FS.ReadByte();
                    }

                    if (PreamblesAreEqual)
                    {
                        Result = UnicodeEncodings[i];
                    }
                }
            }
            catch (System.IO.IOException)
            {
            }
            finally
            {
                if (FS != null)
                {
                    FS.Close();
                }
            }

            if (Result == null)
            {
                Result = Encoding.Default;
            }

            return Result;
        }

        public bool CheckDirectories()
        {
            string dirInput = Properties.Settings.Default.InputPath;
            string dirOutput = Properties.Settings.Default.OutputPath;
            string dirDictionary = Properties.Settings.Default.DictionaryPath;
            string dirError = Properties.Settings.Default.ErrorPath;
            string dirDone = Properties.Settings.Default.DonePath;
            string dirLog = Properties.Settings.Default.LogPath;

            try
            {

                // string Sourcename = this.ServiceName;

                // Adding the files in an array that we can loop through later
                ArrayList dir = new ArrayList();
                dir.Add(dirInput);
                dir.Add(dirOutput);
                dir.Add(dirDictionary);
                dir.Add(dirError);
                dir.Add(dirDone);
                dir.Add(dirError);

                // Checking if directories exists and if they don't we create them
                message = "Starting creating or checking if directories exists!";

                LogFile.WriteLog(ref dirLog, ref message);

                bool tmp = true;
                // Looping through the directories array
                foreach (String Dirs in dir)
                {
                    // if the directory does not exists, we shall create it
                    if (!Directory.Exists(Dirs))
                    {
                        if (Dirs != null)
                        {
                            message = "Creating " + Dirs;
                            LogFile.WriteLog(ref dirLog, ref message);

                            Directory.CreateDirectory(Dirs);
                        }
                        else
                        {
                            
                            tmp = false;


                        }

                    }
                    else
                    {
                        message= Dirs + " exists.";
                        LogFile.WriteLog(ref dirLog, ref message);
                    }

                }
                // We have created directories and so we return true
                // This shall be inserted into the config-file so that we
                // don't have to check if all directories exists
                return tmp;
            }
            catch (IOException ioe)
            {
                // Catching the exception and putting the output in the 
                // error logfile
                Exception exception = ioe;
                string message = "An IO Exception has occured";
                LogFile.WriteErr(ref dirLog, ref message, ref exception);

                return false;

            }
            catch (Exception e)
            {
                string message = "An Exception has occured";
                LogFile.WriteErr(ref dirLog, ref message, ref e);

                return false;
            }
        }


        

	}
}
