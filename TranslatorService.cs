#define DEBUG
using System;
using System.Threading;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO;
using System.Timers;
using System.Globalization;
using ntb_FuncLib;



namespace SportsTranslator
    
{
    

	public class TranslatorService : System.ServiceProcess.ServiceBase
        
    {        
        private IContainer components;

        // Separating some directories so that we have capitalized and not capitalized

        // This is the directories where files enters and leaves
        // These should be found in the config-file
        public static string dirInput;        // This where the files arrives
        public static string dirOutput;       // This is where we send the files
        public static string dirDictionary;   // This is where we have the dictionary
        
        // The following directories are system necessary
        public static string dirLog;      // This is where you'll find the log-file
        public static string dirDone;     // This is where we put the files that has been translated
        public static string dirError;    // This is where we send files that are errorous

        // This is where you'll find the original files
        private System.Windows.Forms.Timer timerTranslater;     

        // Initialize the timer class
        System.Timers.Timer timer = new System.Timers.Timer();

        // Strings used for errors and other messages
        public string strErrorMsg = "";
        public string strMessage = "";

        public CultureInfo ci = new CultureInfo("nb-NO");

        io IO = new io();
        

        
        public TranslatorService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(WorkerThreadHandler);

            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
			// TODO: Add any initialization after the InitComponent call

            
            
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] 
            { 
                
                new TranslatorService() 
            };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);

            
            

		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.timerTranslater = new System.Windows.Forms.Timer(this.components);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

        


        public void WorkerThreadHandler(object sender, UnhandledExceptionEventArgs args)
        {
            if (!(args.ExceptionObject is ThreadAbortException))
            {
                Exception exc = args.ExceptionObject as Exception;

                // This might cause an error or two
                strErrorMsg = "An error has occured:";
                LogFile.WriteErr (ref dirLog, ref strErrorMsg, ref exc);
            }
        }

        
        

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
        /// 
        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.

            string message = "";

            // bool DirectoriesExists = IO.CheckDirectories();

            // Set the timer for the service
            setTimer();

            // Properties.Settings.Default.CreatedDirectories = DirectoriesExists;

            try
            {
                // Start the translator
                translator trans = new translator();
            }
            catch (Exception ex)
            {
                // Writing error to error log
                message = "An error has occured";
                LogFile.WriteErr(ref dirLog, ref message, ref ex);

                if (ex.InnerException.ToString() != "")
                {
                    Exception InnerException = ex.InnerException;
                    string strIE = "An inner exception has occured";
                    
                    LogFile.WriteErr(ref dirLog, ref strIE, ref InnerException);
                }
            }
        
            //
            // This part here will grab all other exceptions
            // 

            AppDomain.CurrentDomain.UnhandledException += 
                new UnhandledExceptionEventHandler(
                delegate(object sender, UnhandledExceptionEventArgs e)
                {
                    if (e.IsTerminating)
                    {
                        object o = e.ExceptionObject;

                        // Write string to error log file
                        string strO = "The following unhandled Exception has occured!";
                        strO += o.ToString();
                        
                        LogFile.WriteLog(ref dirLog, ref strO);
                  }
              }
            );
        }
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
            // TODO: Add code here to perform any tear-down necessary to stop your service.

            strMessage = "Stopping the SportsTranslator";
            timer.Enabled = false;
            
		}

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            translator trans = new translator();

            try
            {
                

                // We are running the translator and translating files
                // trans.TranslateFiles();
                trans.TranslateJob();
            }
            catch (Exception ex)
            {
                // strMessage = "Exception: " + ex.ToString();
                LogFile.WriteErr (ref dirLog, ref strMessage, ref ex);

                if (ex.InnerException.ToString() != "")
                {
                    // strMessage = "InnerException: " + ex.InnerException.ToString();
                    strMessage = "An inner Exception has occured!";
                    Exception InnerException = ex.InnerException;
                    LogFile.WriteErr(ref dirLog, ref strMessage, ref InnerException);
                }


            }
        }

        private bool CheckDirectories()
        {
            // myFolderCollection.AddRange (Properties.Settings.Default.Inputs);
            folders f = new folders();

            /*
            foreach (string directories in Properties.Settings.Default.Inputs)
            {
                f.setFolders(directories);
            }
             */

            dirLog = f.LogDirectory;
            DirectoryInfo di = new DirectoryInfo(f.LogDirectory);

            if (!di.Exists)
            {
                di.Create();
            }

            string dirInput = f.InputDirectory;     // Properties.Settings.Default.DirectoryInput;
            string dirOutput = f.OutputDirectory;   // Properties.Settings.Default.DirectoryOutput;
            string dirDictionary = f.DicDirectory;  // Properties.Settings.Default.DirectoryDictionary;
            string dirError = f.ErrorDirectory;     // Properties.Settings.Default.DirectoryError;
            string dirDone = f.DoneDirectory;       // Properties.Settings.Default.DirectoryDone;

            try
            {

                // string Sourcename = this.ServiceName;

                // Adding the files in an array that we can loop through later
                ArrayList dir = new ArrayList();
                dir.Add(f.InputDirectory);
                dir.Add(f.OutputDirectory);
                dir.Add(f.LogDirectory);
                dir.Add(f.DicDirectory);
                dir.Add(f.DoneDirectory);
                dir.Add(f.ErrorDirectory);

                // Checking if directories exists and if they don't we create them
                strMessage = "Starting creating or checking if directories exists!";

                LogFile.WriteLog(ref dirLog, ref strMessage);

                bool tmp = true;
                // Looping through the directories array
                foreach (String Dirs in dir)
                {
                    // if the directory does not exists, we shall create it
                    if (!Directory.Exists(Dirs))
                    {
                        if (Dirs != null)
                        {
                            strMessage = "Creating " + Dirs;
                            LogFile.WriteLog(ref dirLog, ref strMessage);

                            di = new DirectoryInfo(Dirs);
                            di.Create();
                        }
                        else
                        {
                            // string str = "String was empty!";
                            tmp = false;


                        }

                    }
                    else
                    {
                        strMessage = Dirs + " exists.";
                        LogFile.WriteLog(ref dirLog, ref strMessage);
                    }

                }
                // We have created directories and so we return true
                // This shall be inserted into the config-file so that we
                // don't have to check if all directories exists
                return tmp;
            }
            catch (IOException ioe)
            {
                // Catching the exception and putting the output in the 
                // error logfile
                Exception exception = ioe;
                string message = "An IO Exception has occured";
                LogFile.WriteErr(ref dirLog, ref message, ref exception);
                
                return false;

            }
            catch (Exception e)
            {
                string message = "An Exception has occured";
                LogFile.WriteErr(ref dirLog, ref message, ref e);

                return false;
            }
        }

        protected void setTimer()
        {
            // Getting the timer interval from the settings file
            #if (DEBUG)
            strMessage = "Enabling timer, a system timer with interval of the value set in the app.config-file";

            
            LogFile.WriteLog(ref dirLog, ref strMessage);
            #endif

            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

            timer.Interval = Properties.Settings.Default.TimerInterval;

            //ad 3: enabling the timer
            timer.Enabled = true;

        }

	}
}