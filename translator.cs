#define DEBUG

using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Diagnostics;
using System.Management;
using System.Globalization;
using ntb_FuncLib;



namespace SportsTranslator
{
	/// <summary>
	/// Summary description for translator.
	/// </summary>
	public class translator
	{

        // This holds the dictionary
		public Hashtable arrDictionary = new Hashtable();

        // Creating the logfile
        private static LogFile logFile = new LogFile();
        

        private static TranslatorService ts = new TranslatorService();

        private static string dirInput = Properties.Settings.Default.InputPath;
        private static string dirOutput = Properties.Settings.Default.OutputPath;
        private static string dirDictionary = Properties.Settings.Default.DictionaryPath;
        private static string dirLog = Properties.Settings.Default.LogPath;
        private static string dirError = Properties.Settings.Default.ErrorPath;
        private static string dirDone = Properties.Settings.Default.DonePath;
        
        private string message = "";



        /// <summary>
        /// The constructor for this class. Doesn't do much really.
        /// But we can make it read the dictionary file, which means it will be 
        /// read every two minutes... Do we want that?
        /// </summary>
        public translator()
        {
            
            #if (DEBUG)
            message = "Reading dictionary file";
            LogFile.WriteLog(ref dirLog, ref message);
            #endif

            // We read the dictionary file
            readDictionary();
        }

        
        /// <summary>
        /// This method shall be called when we start up the service
        /// </summary>
        public void Start()
        {
            //
        }

        /// <summary>
        /// Call this function if you want to run the TranslateFiles with subdirectories
        /// </summary>
        public void TranslateJob()
        {
            /*
            foreach (string directories in Properties.Settings.Default.Inputs)
            {
                this.TranslateFiles(directories);
            }
             */
        }

        /// <summary>
        /// This method is run if we are checking files in subdirectories
        /// </summary>
        /// <param name="job"></param>
        public void TranslateFiles(string job)
        {
            // Process the list of files found in the directory.
            try
            {
                message = "In TranslateFiles(string job)";
                LogFile.WriteLog(ref dirLog, ref message);

                message = "Creating IO-object from TranslateFiles";
                LogFile.WriteLog(ref dirLog, ref message);

                // Creating the IO-object
                io IO = new io();


                // Telling where we are in the world
                message = "TranslatorService Object";
                LogFile.WriteLog(ref dirLog, ref message);

                // Getting the subdirectory that we shall place the files in

                this.debugFileStrings();

                IO.getDirectoryNames();
                string tmpInput = "";
                string tmpOutput = "";
                string tmpDone = "";

                
                tmpInput = dirInput + @"\" + job;
                tmpOutput = dirOutput + @"\" + job;
                tmpDone = dirDone + @"\" + job;

                // This is just debug information
                #if (DEBUG) 
                message = "This is dirInput: " + tmpInput;
                LogFile.WriteLog(ref dirLog, ref message);
                message += "This is dirOutput: " + tmpOutput;

                LogFile.WriteLog(ref dirLog, ref message);
                #endif 

                if (tmpInput != "")
                {
                    string[] fileEntries = Directory.GetFiles(tmpInput);

                    int totalFiles = fileEntries.Length;

                    #if (DEBUG)
                    message = "A total of " + totalFiles.ToString() + " files will be translated";
                    LogFile.WriteLog(ref dirLog, ref message);

                    #endif
                    DateTime StartTime = DateTime.Now;



                    foreach (string fileName in fileEntries)
                    {
                        // do something with fileName
                        #if (DEBUG)
                        message = fileName + " is loaded and will be translated";
                        LogFile.WriteLog(ref dirLog, ref message);
                        #endif
                        string content = IO.readFile(fileName);

                        // Translating file
                        content = this.translate(content);

                        // Write to new file - which is in Output directory
                        message = fileName + " is translated and will be saved";
                        LogFile.WriteLog(ref dirLog, ref message);

                        // We are to check if \ is in the filename
                        // We pretty much know that it is, 
                        // so I split it and use the last one

                        string[] arrFilename = fileName.Split((@"\").ToCharArray());

                        string saveFile = tmpOutput + @"\" + arrFilename[arrFilename.Length - 1];
                        string doneFile = tmpDone + @"\" + arrFilename[arrFilename.Length - 1];

                        // evt.WriteEntry(saveFile + " is the new name");
                        #if (DEBUG)
                        message = saveFile + " is the new name";
                        LogFile.WriteLog(ref dirLog, ref message);

                        message += doneFile + " is where we put the original file";
                        LogFile.WriteLog(ref dirLog, ref message);
                        #endif

                        // Writing the new file
                        IO.writeFile(saveFile, content);

                        // Moving the done file
                        IO.MoveFile(fileName, doneFile);

                        // Emptying string
                        content = "";

                    }
                    DateTime EndTime = DateTime.Now;

                    TimeSpan duration = EndTime - StartTime;

                    message = "It took " + duration.Seconds.ToString() + " seconds to translate " + totalFiles.ToString() + " files";
                    LogFile.WriteLog(ref dirLog, ref message);

                    #if (DEBUG)
                    if ((duration.Seconds.ToString() == "0") && (totalFiles.ToString() == "0"))
                    {
                        message = "And you call me stupid! Asking me to translate no files at all!";
                    }
                    else
                    {

                        message = "I may be stupid, but I sure am fast!";
                    }

                    LogFile.WriteLog(ref dirLog, ref message);
                    #endif
                }
                else
                {
                    message = "dirInput is empty: " + dirInput;
                    LogFile.WriteLog(ref dirLog, ref message);
                }
            }
            catch (Exception ex)
            {
                message = "An error has occured!";
                

                LogFile.WriteErr(ref dirLog, ref message, ref ex);

                // Checking if there are more information that we can get
                if (ex.InnerException.ToString() != "")
                {
                    message = "An innerexception has occured";
                    Exception InnerException = ex.InnerException;

                    LogFile.WriteErr(ref dirLog, ref message, ref InnerException);
                }
            }

        }

        public void TranslateFiles () {
            // Process the list of files found in the directory.
            try
            {

                #if (DEBUG)
                message = "Creating IO-object from TranslateFiles";
                LogFile.WriteLog(ref dirLog, ref message);

                // Telling where we are in the world
                message = "TranslatorService Object";
                LogFile.WriteLog(ref dirLog, ref message);

                message = "This is dirInput: " + dirInput;
                LogFile.WriteLog(ref dirLog, ref message);
                message = "This is dirOutput: " + dirOutput ;
                LogFile.WriteLog(ref dirLog, ref message);
                #endif
                this.debugFileStrings();


                // Creating the IO-object
                io IO = new io();

                if (dirInput != "")
                {
                    string[] fileEntries = Directory.GetFiles(dirInput);

                    int totalFiles = fileEntries.Length;
                    message = "A total of " + totalFiles.ToString() + " files will be translated";
                    LogFile.WriteLog(ref dirLog, ref message);

                    DateTime StartTime = DateTime.Now;

                    foreach (string fileName in fileEntries)
                    {
                        // do something with fileName
                        message = fileName + " is loaded and will be translated";
                        LogFile.WriteLog(ref dirLog, ref message);
                        string content = IO.readFile(fileName);
                        ArrayList arrContent = IO.ReadIntoArray(fileName);

                        // Translating file
                        // content = this.translate(content);
                        content = "";
                        foreach (string temp in arrContent) {
                            content += translate(temp);

                        }

                        if (content != "")
                        {
                            message = fileName + " is translated and will be saved";
                            LogFile.WriteLog(ref dirLog, ref message);

                            // We are to check if \ is in the filename
                            // We pretty much know that it is, so I split it and use the last one

                            string[] arrFilename = fileName.Split((@"\").ToCharArray());

                            string saveFile = dirOutput + @"\" + arrFilename[arrFilename.Length - 1];
                            string doneFile = dirDone + @"\" + arrFilename[arrFilename.Length - 1];

                            // evt.WriteEntry(saveFile + " is the new name");
                            message = saveFile + " is the new name\n";
                            message += doneFile + " is where we put the original file";

                            LogFile.WriteLog(ref dirLog, ref message);
                            // IO.writeFile(fileName, content);

                            // And we shall move the translated file to the done-directory
                            // IO.moveFile(fileName);
                            // Writing the new file
                            IO.writeFile(saveFile, content);

                            // Moving the done file
                            IO.MoveFile(fileName, doneFile);

                            // Emptying string
                            content = "";
                        }

                    }
                    DateTime EndTime = DateTime.Now;

                    TimeSpan duration = EndTime - StartTime;

                    message = "It took " + duration.Seconds.ToString() + " seconds to translate " + totalFiles.ToString() + " files";
                    LogFile.WriteLog(ref dirLog, ref message);

                    #if (DEBUG)
                    if ((duration.Seconds.ToString() == "0") && (totalFiles.ToString() == "0"))
                    {
                        message = "And you call me stupid! Asking me to translate no files at all!";
                    }
                    else
                    {

                        message = "I may be stupid, but I sure am fast!";
                    }

                    LogFile.WriteLog(ref dirLog, ref message);
                    #endif
                }
                else
                {
                    message = "dirInput is empty: " + dirInput;
                    LogFile.WriteLog(ref dirLog, ref message);
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();

                LogFile.WriteLog(ref dirLog, ref message);

                // Checking if there are more information that we can get
                if (ex.InnerException.ToString() != "")
                {
                    message = ex.InnerException.ToString();

                    LogFile.WriteLog(ref dirLog, ref message);
                }
            }
        }

        /// <summary>
        /// This method reads the dictionary file and returns the file as an array
        /// </summary>
        public void readDictionary()
        {
            // TODO: We shall create a flag/file which says when the dictionary-file was read
        #if (DEBUG)
            message = "In readDictionary method: Reading Dictionary file";
            LogFile.WriteLog(ref dirLog, ref message);

            message = "Dictionary dir is: " + dirDictionary;
            LogFile.WriteLog(ref dirLog, ref message);

            message = "And directoryfile is: " + dirDictionary;
            LogFile.WriteLog(ref dirLog, ref message);

            message = "In readDictionary method: Checking if file exists. If not - Create one";
            LogFile.WriteLog(ref dirLog, ref message);
        #endif
            

            io IO = new io();

            // Getting the filename of the dictionary
            string DicFilename = dirDictionary + @"\" + Properties.Settings.Default.FileDictionary; 

            if (IO.FileExists(DicFilename) == false)
            {
                message = "Dictionary file does not exists. Creating one!";
                LogFile.WriteLog(ref dirLog, ref message);
                
                // We create the file empty
                IO.writeFile(DicFilename, "");
            }

            arrDictionary = null;

            #if (DEBUG)
            message = "In readDictionary method: Adding file content to array";
            LogFile.WriteLog(ref dirLog, ref message);
            #endif 

            arrDictionary = IO.ReadFileToArray(DicFilename);

            // return arrDictionary;
            #if (DEBUG)
            
            message = "In readDictionary method: Done Dictionary file";
            LogFile.WriteLog(ref dirLog, ref message);

            #endif
        }
		

        public void Main () {
            // string dirLog = dirLog;
        }


        public void debugFileStrings()
        {
            #if (DEBUG)
            string[] strDebug = new string[] {"Input: "+dirInput,
                "Output: "+dirOutput,
                "Dictionary: "+dirDictionary,
                "dirLog: "+dirLog,
                "dirError: "+dirError,
                "dirDone: "+dirDone};

            foreach (string debugDirectories in strDebug)
            {
                string tmp = debugDirectories;
                LogFile.WriteLog(ref dirLog, ref tmp);
            }
            #endif
        }

		/// <summary>
		/// This method shall find the word and - if it is in the dictionary-file
		/// we shall replace the name with other parameters
		/// </summary>
		/// <param name="content">String containing the file we are replacing words in</param>
        public string translate(string content)
        {
            #if (DEBUG)
            message = "In translator.translate starting to translate file";
            LogFile.WriteLog(ref dirLog, ref message);
            #endif
            
            // We should either tell that we are converting files here,
            // ... or we should stop the timer while we are translating this 
            // bunch of files


            // Adding a check to see if arrDictionary holds any data
            if (arrDictionary.Count == 0)
            {
                this.readDictionary();
            }


            // Loops through the items in the array
            foreach (DictionaryEntry Item in arrDictionary)
            {
                string strTemp = Item.Key.ToString();
                string temp2 = strTemp.Replace(@"(", @"\(");
                string temp3 = temp2.Replace(@")", @"\)");
                strTemp = temp3;
                // Doing the replace
                string regexpattern = @"\b" + strTemp + @"\b";
                LogFile.WriteLogNoDate(ref dirLog, ref regexpattern);
                strTemp = "";
                if (Regex.IsMatch(content, regexpattern))
                {
                    content = Regex.Replace(content, regexpattern, Item.Value.ToString());
                    LogFile.WriteLog(ref dirLog, ref regexpattern);
                }

                regexpattern = @"([0-9]{1}) ([0-9]{3})";
                if (Regex.IsMatch(content, regexpattern))
                {
                    content = Regex.Replace(content, regexpattern, "$1$2");
                }
                else

                    // above 10000 = 10.000
                    regexpattern = @"([0-9]{2}) ([0-9]{3})";
                if (Regex.IsMatch(content, regexpattern))
                {
                    content = Regex.Replace(content, regexpattern, "$1.$2");
                }

            }
            return content;
        }        

	}
}