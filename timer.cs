﻿using System;
using System.Timers;
using System.Diagnostics;
using ntb_FuncLib;

namespace SportsTranslator
{
    public class timer
    {
        // private static System.Timers.Timer aTimer;

        
        private static LogFile logFile = new LogFile();
        private static TranslatorService ts = new TranslatorService();

        // I kinda hate that we have to get these properties all the time
        // Cutting it down to the only needed variables from the configuration file
        private static string dirLog = Properties.Settings.Default.LogPath;
        private static int interval = Properties.Settings.Default.TimerInterval;
        

        /// <summary>
        /// This method control everything that has to do with the timer class
        /// The class is used to poll every X seconds, where X is the value
        /// found in the application configuration file.
        /// </summary>
        public static void Main ()
        {
           
            // Create a timer with a ten second interval
            System.Timers.Timer aTimer = new System.Timers.Timer();

            // Hook up the Elapsed event for the timer
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            // Set the timer interval to two minutes (120000 milliseconds)
            // or what we have set in the configuration file 
            aTimer.Interval = interval;

            // Enable Timer
            aTimer.Enabled = true;

            // Keep the timer alive until the end of Main.
            GC.KeepAlive(aTimer);

        }

        public static void OnTimedEvent(object source, ElapsedEventArgs e)
        {


        }
    }
}
