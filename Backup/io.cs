using System;
using System.IO;
using System.Collections;



namespace SportsTranslator
{
	/// <summary>
	/// This class deals with everything that has to do with IO.
	/// For instance creating directories, loading files, saving files and so on
	/// </summary>
	public class io
	{
		public io()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// This method reads the content of the file
		/// </summary>
		/// <param name="Filename">String containing the name of the file to be read</param>
		/// <returns>Content of the file</returns>
		public string readFile (string Filename) 
		{
			if (!File.Exists(Filename) )
			{
				throw (new FileNotFoundException(
					Filename + " cannot be read since it does not exist.", Filename));
			}

			string contents = "";

			using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
					   FileAccess.Read, FileShare.None)) 
			{
				using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.Default)) 
				{
					contents = streamReader.ReadToEnd();
				}
			}

			return contents;


		}

		/// <summary>
		/// This method writes the file to the directory
		/// </summary>
		/// <param name="Filename">String containing the filename</param>
		/// <param name="data">String containing the data that shall be written to the file</param>
		public void writeFile (string Filename, string data) 
		{
			using (FileStream fileStream = new FileStream(Filename, FileMode.CreateNew,
					   FileAccess.Write, FileShare.None))
			{
				using (StreamWriter streamWriter = new StreamWriter(fileStream)) 
				{
					streamWriter.WriteLine(data);
				}
			}
		}

		/// <summary>
		/// This method checks if the file exists or not
		/// </summary>
		/// <param name="Filename">Filename we are to check existance of</param>
		/// <returns>Returns false if the file exists, otherwize true</returns>
		public bool FileExists (string Filename) 
		{
			if (File.Exists(Filename)) 
			{
				return true;
			} 
			return false;
		}

		/// <summary>
		/// This method reads the content of a file into an array/collection
		/// </summary>
		/// <param name="Filename">name of the file we are to load</param>
		/// <returns>Returns the content of the file in an array</returns>
		public Hashtable ReadFileToArray (string Filename) 
		{
			if (!File.Exists(Filename) )
			{
				throw (new FileNotFoundException(
					Filename + " cannot be read since it does not exist.", Filename));
			}

			string strContents = "";
			Hashtable dictionary = new Hashtable();

			using (FileStream fileStream = new FileStream(Filename, FileMode.Open,
					   FileAccess.Read, FileShare.None)) 
			{
				using (StreamReader streamReader = new StreamReader(fileStream, System.Text.Encoding.Default))
				{
					while (streamReader.Peek() != -1) 
					{
						/*
						 * We know that the format of the line is like this:
						 * Word1:ReplaceWord1
						 */
						strContents = streamReader.ReadLine();
						string[] strWords = strContents.Split(':');
						dictionary.Add(strWords[0], strWords[1]);

						
					}
				}
			}

			return dictionary;

		}

		/// <summary>
		/// This method creates a directory if it does not exists
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void createDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;
			if (!Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);
				dirInfo.Create();
			}
		}

		/// <summary>
		/// This method checks if the directory exists or not
		/// </summary>
		/// <param name="DirectoryName">Name of directory to check - complete path</param>
		/// <returns>Returns false if the directory does not exists, otherwize true</returns>
		public bool getDirectory (string DirectoryName) 
		{
			if (!Directory.Exists(@DirectoryName)) 
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// This method deletes the directory and subdirectories within
		/// </summary>
		/// <param name="DirectoryName">String containing the name of the directory - complete path</param>
		/// <returns></returns>
		public void deleteDirectory (string DirectoryName) 
		{
			DirectoryInfo dirInfo = null;

			if (Directory.Exists(@DirectoryName)) 
			{
				dirInfo = new DirectoryInfo(@DirectoryName);

				dirInfo.Delete(true);
			}
		}

	}
}
